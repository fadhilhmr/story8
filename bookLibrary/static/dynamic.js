$(document).ready(function() {
    $("#isi").keyup(function() {
        var q = $(this).val().toLowerCase()
        console.log(q)
        $.ajax({
            url: '/data?q=' + q+'/',
            success: function(data) {
                $('#konten').html('')
                for (var i = 0; i < data.items.length; i++) {
                    var result = '';
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' id='foto' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td> </tr>"
                        $('#konten').append(result);
                }
            },
            error: function(error) {
                $('#konten').append("<h1>Buku Tidak Tersedia.</h1>");
            }
        })
    });
});
