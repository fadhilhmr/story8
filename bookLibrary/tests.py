from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from bookLibrary.views import index, data
import time

    
class Story8UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_json_url(self):
        response = Client().get('/data')
        self.assertEqual(response.status_code, 200)
    def test_using_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_using_data(self):
        found = resolve('/data')
        self.assertEqual(found.func, data)
    # def test_

# class Story8FuncionalTest():
#     def tearUp():

   
  
    
    




